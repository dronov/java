package ru.ifmo.ctddev.dronov.task2;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 05.03.14
 * Time: 23:36
 */

public class ArraySet<E> extends AbstractSet<E> implements NavigableSet<E> {

    private final List<E> list;
    private Comparator<? super E> comparator;

    public ArraySet() {
        list = new ArrayList<E>();
        comparator = null;
    }

    private ArraySet(List<E> list, Comparator<? super E> comparator) {
        this.list = list;
        this.comparator = comparator;
    }

    public ArraySet(Collection<E> collection) {
        this(collection, new Comparator<E>() {
            @Override
            public int compare(E o1, E o2) {
                return ((Comparable<? super E >) o1).compareTo(o2);
            }
        });
        comparator = null;
    }

    public ArraySet(Collection<E> collection, Comparator<? super E> comparator) {
        List<E> currentList = new ArrayList<E>();
        currentList.addAll(collection);
        Collections.sort(currentList, comparator);
        List<E> uniqueValues = new ArrayList<E>();
        if (!currentList.isEmpty())
            uniqueValues.add(currentList.get(0));

        for (E object : currentList) {
            if (comparator.compare(uniqueValues.get(uniqueValues.size() - 1), object) != 0) {
                uniqueValues.add(object);
            }
        }
        this.list = uniqueValues;
        this.comparator = comparator;
    }


    private int binarySearch(List<E> list, E findElement, Comparator<? super E> comparator) {
        return Collections.binarySearch(list, findElement, comparator);
    }

    private int binarySearchSet(E toElement, boolean inclusive) {
        int index = binarySearch(list, toElement, comparator);
        if (inclusive && index >= 0) {
            index++;
        } else if (index < 0) {
            index = -index - 1;
        }
        return index;
    }


    @Override
    public E lower(E e) {
        int index = binarySearch(list, e, comparator);
        if (index < 0)
            index = -index - 1;
        return (index == 0 ? null : list.get(index - 1));
    }

    @Override
    public E floor(E e) {
        int index = binarySearch(list, e, comparator);
        if (index >= 0)
            return list.get(index);
        else index = -index - 1;
        return (index == 0 ? null : list.get(index - 1));
    }

    @Override
    public E ceiling(E e) {
        int index = binarySearch(list, e, comparator);
        if (index >= 0)
            return list.get(index);
        else index = -index - 1;
        return (index == list.size() ? null : list.get(index));
    }

    @Override
    public E higher(E e) {
        int index = binarySearch(list, e, comparator);
        if (index >= 0)
            return (index == list.size() - 1 ? null : list.get(index + 1));
        else index = -index - 1;
        return (index == list.size() ? null : list.get(index));
    }

    @Override
    public E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @Override
    public E pollLast() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            Iterator<E> currentIterator = list.iterator();
            @Override
            public boolean hasNext() {
                return currentIterator.hasNext();
            }
            @Override
            public E next() {
                return currentIterator.next();
            }
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public NavigableSet<E> descendingSet() {
        List<E> currentList = new ArrayList<>(list);
        Collections.reverse(currentList);
        return new ArraySet<E>(currentList, Collections.reverseOrder(comparator));
    }

    @Override
    public Iterator<E> descendingIterator() {
        return descendingSet().iterator();
    }

    @Override
    public NavigableSet<E> subSet(E fromElement, boolean fromInclusive, E toElement, boolean toInclusive) {
        return headSet(toElement, toInclusive).tailSet(fromElement, fromInclusive);
    }

    @Override
    public NavigableSet<E> headSet(E toElement, boolean inclusive) {
        int index = binarySearchSet(toElement, inclusive);
        return new ArraySet<E>(list.subList(0, index), comparator);
    }

    @Override
    public NavigableSet<E> tailSet(E fromElement, boolean inclusive) {
        int index = binarySearchSet(fromElement, !inclusive);
        return new ArraySet<E>(list.subList(index, list.size()), comparator);
    }

    @Override
    public Comparator<? super E> comparator() {
        return comparator;
    }

    @Override
    public SortedSet<E> subSet(E fromElement, E toElement) {
        return subSet(fromElement, true, toElement, false);
    }

    @Override
    public SortedSet<E> headSet(E toElement) {
        return headSet(toElement, false);
    }

    @Override
    public SortedSet<E> tailSet(E fromElement) {
        return tailSet(fromElement, true);
    }

    @Override
    public E first() {
        if (list.isEmpty())
            throw new NoSuchElementException();
        return list.get(0);
    }

    @Override
    public E last() {
        if (list.isEmpty())
            throw new NoSuchElementException();
        return list.get(list.size() - 1);
    }
}
