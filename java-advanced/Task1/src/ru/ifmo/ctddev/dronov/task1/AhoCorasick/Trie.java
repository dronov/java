package ru.ifmo.ctddev.dronov.task1.AhoCorasick;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 18.02.14
 * Time: 22:59
 * To change this template use File | Settings | File Templates.
 */

public class Trie {

    private final int alphabet;

    private Node root, currentNode;
    private String isResultFound = null;

    public Trie(int alphabet) {
        this.alphabet = alphabet;
        this.root = new Node(null, -1);
        reset();
    }

    public void addWord(byte[] word, String encoding) {
        Node current = root;
        for (int i = 0; i < word.length; i++) {
            int currentByte = getAlphabetModule(word[i]);
            if (current.son[currentByte] == null) {
                current.son[currentByte] = new Node(current, currentByte);
            }
            current = current.son[currentByte];
        }
        current.isLeaf = true;
        current.encoding = encoding;
    }

    private int getAlphabetModule(int character) {
        return (character + alphabet) % alphabet;
    }

    public void reset() {
        currentNode = root;
        isResultFound = null;
    }

    public void processText(int currentByte) {
        int character = getAlphabetModule(currentByte);
        Node current = currentNode.getGo(currentNode, character);
        currentNode = current;
        while (current != root) {
            if (isResultFound == null)
                isResultFound = current.encoding;
            current = currentNode.getUp(current);
        }
    }

    public String getResult() {
        return isResultFound;
    }

    private class Node {
        Node[] son = new Node[alphabet];
        Node[] go = new Node[alphabet];

        Node parent;
        Node suffLink;
        Node up;
        int charToParent;
        boolean isLeaf;
        private String encoding;

        public Node(Node parent, int character) {
            this.parent = parent;
            this.charToParent = character;
            this.suffLink = null;
            this.up = null;
            isLeaf = false;
        }

        private Node getSuffLink(Node current) {
            if (current.suffLink == null) {
                if (current == root || current.parent == root) {
                    current.suffLink = root;
                } else {
                    current.suffLink = getGo(getSuffLink(current.parent), current.charToParent);
                }
            }
            return current.suffLink;
        }

        private Node getGo(Node current, int character) {
            if (current.go[character] == null) {
                if (current.son[character] != null) {
                    current.go[character] = current.son[character];
                } else {
                    current.go[character] = (current == root) ? root : getGo(getSuffLink(current), character);
                }
            }
            return current.go[character];
        }

        private Node getUp(Node current) {
            if (current.up == null) {
                if (getSuffLink(current).isLeaf) {
                    current.up = getSuffLink(current);
                } else if (getSuffLink(current) == root) {
                    current.up = root;
                } else  {
                    current.up = getUp(getSuffLink(current  ));
                }
            }
            return current.up;
        }
    }
}
