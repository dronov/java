package ru.ifmo.ctddev.dronov.task1;

import ru.ifmo.ctddev.dronov.task1.AhoCorasick.Trie;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 17.02.14
 * Time: 1:06
 * To change this template use File | Settings | File Templates.
 */

public class Grep extends SimpleFileVisitor<Path> {

    public static final String DEFAULT_ENCODE = "UTF-8";
    public static final int ALPHABET = 256;
    public static final int BUFFER_SIZE = 512;
    public final String CONSOLE_SEPARATOR = "-";


    private String[] encodings = {"UTF-8", "KOI8-R", "CP1251", "CP866"};
    private List<String> list;
    private Trie trie;

    public Grep() {}
    public Grep(Trie trie) {
        this.trie = trie;
    }

    private void run(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in, DEFAULT_ENCODE));
            if (args.length == 0) {
                System.out.println("Input Format Exception");
            } else if (args.length == 1 && args[0].equals(CONSOLE_SEPARATOR)) {
                String str = br.readLine();
                list = new ArrayList<String>();
                while (str != null && !str.isEmpty()) {
                    list.add(str);
                    str = br.readLine();
                }
            } else {
                list = Arrays.asList(args);
            }
            execute(list);

        } catch (IOException e) {
            System.out.println("Input format exception");
        } finally {

        }
    }

    private void execute(List<String> list) throws IOException {
        createTrie(list);
        Path startingDir = Paths.get("");
        Grep instance = new Grep(trie);
        Files.walkFileTree(startingDir, instance);
    }

    private void createTrie(List<String> list) throws UnsupportedEncodingException {
        trie = new Trie(ALPHABET);
        for (String word : list) {
            for (String encoding : encodings) {
                byte[] bytes = word.getBytes(encoding);
                trie.addWord(bytes, encoding);
            }
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        try {
            trie.reset();
            InputStream in = new BufferedInputStream(new FileInputStream(file.toFile()));
            byte[] buffer = new byte[BUFFER_SIZE];
            int size = 0;
            int currentByte = in.read();
            boolean isLineLong = false;
            while (currentByte != -1) {
                char character = (char)currentByte;
                if (character == '\r' || character == '\n') {
                    size = 0;
                    isLineLong = false;
                    trie.reset();
                } else {
                    if (size >= BUFFER_SIZE) {
                        isLineLong = true;
                        size %= BUFFER_SIZE;
                    }
                    buffer[size] = (byte)currentByte;
                    size++;
                }
                trie.processText(currentByte);
                String result = trie.getResult();
                currentByte = in.read();
                if (result != null) {
                    byte[] resultLine;
                    boolean isEndLine = (currentByte != -1) && (char)currentByte == '\n' || (char)currentByte == '\r';
                    System.out.print(file.toString() + " : ");
                    if (isLineLong) {
                        resultLine = new byte[BUFFER_SIZE];
                        System.arraycopy(buffer, size, resultLine, 0, BUFFER_SIZE - size);
                        System.arraycopy(buffer, 0, resultLine, size, size);
                        System.out.println("..." + new String(resultLine, result) + (isEndLine ? "" : "..."));
                    } else {
                        resultLine = new byte[size];
                        System.arraycopy(buffer, 0, resultLine, 0, size);
                        System.out.println(new String(resultLine, result) + (isEndLine ? "" : "..."));
                    }
                    while (currentByte != -1 && (char)currentByte != '\n' && (char)currentByte != '\r')
                        currentByte = in.read();
                }
            }
        } catch (IOException e) {
            System.out.println("Current file do not exists");
        }
        return FileVisitResult.CONTINUE;
    }

    public static void main(String[] args) {
        new Grep().run(args);
    }

}
