package ru.ifmo.ctddev.dronov.task3;

import info.kgeorgiy.java.advanced.implementor.Impler;
import info.kgeorgiy.java.advanced.implementor.ImplerException;
import info.kgeorgiy.java.advanced.implementor.JarImpler;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 12.03.14
 * Time: 22:08
 * To change this template use File | Settings | File Templates.
 */

/**
 * Imlementor class that generate implementaion of specified class
 */
public class Implementor implements Impler, JarImpler {

    /**
     * Value that stores name specified class
     */
    private String className;

    /**
     * This class is wrapper for {@link Method}. It's need to stored good or bad methods
     * in {@link #dfsMethods(Class, java.util.Map, java.util.Set)}
     */
    class MethodStorage {
        /**
         * Field that represent {@link Method}
         */
        private Method method;

        /**
         * Constructor that memorize {@link Method}
         * @param method that could be stored
         */
        MethodStorage(Method method) {
            this.method = method;
        }

        /**
         * Get {@link Method} from it's wrapper
         * @return {@link Method} of it's wrapper
         */
        Method getMethod() {
            return method;
        }

        /**
         * Set {@link Method} to this wrapper
         * @param method is setting to this wrapper
         */
        void setMethod(Method method) {
            this.method = method;
        }

        /**
         * Compare two classes
         * @param obj
         * @return <tt>true</tt> if this are equals with @param, otherwise false
         */
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MethodStorage) {
                MethodStorage st = (MethodStorage)obj;
                return method.getName().equals(st.getMethod().getName()) &&
                        Arrays.equals(method.getParameterTypes(), st.getMethod().getParameterTypes());
            }
            return false;
        }

        /**
         * Calculate hash code to using it in {@link #equals(Object)}
         * @return hashCode of current method
         */
        @Override
        public int hashCode() {
            return (Arrays.hashCode(method.getParameterTypes()) * 31 + method.getName().hashCode() * 53);
        }
    }

    /**
     * The method would to demermine about what kind of class we get inherited.
     * Depends of token class type, method return different implementaion.
     * @param token class that represent parent of class
     * @return implementaion of inheritance specified token
     */
    private String getInheritance(Class<?> token) {
        if (token.isInterface()) {
            return " implements " + token.getSimpleName();
        } else
            return " extends " + token.getSimpleName();
    }

    /**
     * The method return implementaion of {@link Method} arguments, which prefix must beginning
     * with <tt>arg</tt>.
     * @param arguments array, that contains {@link Method} arguments
     * @return implementaion of {@link Method} arguments
     */
    private String getArgumentsImplement(Class<?>[] arguments) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arguments.length; i++) {
            builder.append(arguments[i].getCanonicalName() + " " + "arg" + i);
            if (i != arguments.length - 1)
                builder.append(",");
        }
        return builder.toString();
    }

    /**
     * Try to get implementaion of exception, which can specified class throw
     * @param exceptions array of specified class's exception
     * @return exception implementaion
     */
    private String getExceptionImplement(Class<?>[] exceptions) {
        if (exceptions.length == 0)
            return "";
        StringBuilder builder = new StringBuilder();
        builder.append(" throws ");
        for (int i = 0; i < exceptions.length; i++) {
            builder.append(exceptions[i].getCanonicalName());
            if (i != exceptions.length - 1)
                builder.append(",");
        }
        return builder.toString();
    }

    /**
     * The method return implementaion of constructor arguments, which prefix must beginning
     * with <tt>arg</tt>.
     * @param arguments array, that contains constructor arguments
     * @return implementaion of constructor arguments
     */
    private String getSuperImplement(Class<?>[] arguments) {
        StringBuilder builder = new StringBuilder();
        builder.append("super(");
        for (int i = 0; i < arguments.length; i++) {
            builder.append("arg" + i);
            if (i != arguments.length - 1)
                builder.append(",");
        }
        builder.append("\n);");
        return builder.toString();
    }

    /**
     * Returns {@link Method} return value. If type is primitive method
     * return <tt>true</tt> if type is boolean and empty string if type void.
     * If return type is object type, method return <tt>null</tt>.
     * @param method that could be implement
     * @return return value of method
     */
    private String getReturnValue(Method method) {
        Class<?> returnType = method.getReturnType();
        if (returnType.isPrimitive()) {
            if (returnType.equals(boolean.class))
                return "true";
            else if (returnType.equals(void.class))
                return "";
            else return "0";
        } else return "null";
    }

    /**
     * Returns the {@link Method} implementaion. Depending of return type, method called
     * {@link #getReturnValue(java.lang.reflect.Method)} to determine return value
     * @param method that could be implement
     * @return Returns the {@link Method} implementaion
     */
    private String getMethodImplement(Method method) {
        StringBuilder builder = new StringBuilder();
        builder.append("public " + method.getReturnType().getCanonicalName() + " " + method.getName() + " " + "(");
        builder.append(getArgumentsImplement(method.getParameterTypes()));
        builder.append(")");
        builder.append("{\n");
        builder.append("return " + getReturnValue(method) + ";\n");
        builder.append("}\n");
        return builder.toString();
    }

    /**
     * Represent implementaion for specified constructor. Implement constructors with
     * arguments. Implement {@link Exception} that constructor could be thrown.
     * @param constructor
     * @return implementaion of specified constructor
     */
    private String getConstructorImplement(Constructor<?> constructor) {
        StringBuilder builder = new StringBuilder();
        builder.append("public" + " " + className + "(");

        Class<?>[] arguments = constructor.getParameterTypes(); // add constructor's arguments;
        builder.append(getArgumentsImplement(arguments));

        builder.append(")\n");

        Class<?>[] exceptions = constructor.getExceptionTypes(); // add constructor's exceptions;
        builder.append(getExceptionImplement(exceptions));

        builder.append("{\n");
        arguments = constructor.getParameterTypes();             //add super method
        builder.append(getSuperImplement(arguments));

        builder.append("}\n");

        return builder.toString();
    }

    /**
     * Returns all declared constructors of specified class.
     * @param token
     * @return array with all constructors of token class
     */
    public Constructor<?>[] getAllConstructors(Class<?> token) {
        return token.getDeclaredConstructors();
    }

    /**
     * Method that represent constructors implementaion. If all constructors are private,
     * it's impossible to implement that class, and we going throw {@link ImplerException}
     * @param token
     * @return implementaion of all constructors of class token
     * @see ImplerException
     */
    private String getConstructors(Class<?> token) throws ImplerException {
        StringBuilder builder = new StringBuilder();
        Constructor<?>[] constructors = getAllConstructors(token);
        boolean isAllConstructorsPrivate = true;
        for (Constructor<?> current : constructors) {
            if (!Modifier.isPrivate(current.getModifiers())) {
                builder.append(getConstructorImplement(current) + "\n");
                isAllConstructorsPrivate = false;
            }
        }
        if (isAllConstructorsPrivate && constructors.length > 0)
            throw new ImplerException("All constructors are private");
        return builder.toString();
    }

    /**
     * Method is recursively going throw all classes, that are parents of specified class.
     * The class method is bad, if it's have bad modificator, that shows the impossibility
     * of implement it.
     * @param token class, wherein we get all methods
     * @param map {@link Map} that contains out methods
     * @param badMethods {@link Set} that contains methods, which impossible to implement
     * @see Map
     */
    private void dfsMethods(Class<?> token, Map<MethodStorage, Method> map, Set<MethodStorage> badMethods) {
        if (token == null || token.isPrimitive() || token.isArray() || token.isMemberClass())
            return;

        Method[] currentMethods = token.getDeclaredMethods();
        for (Method method : currentMethods) {
            MethodStorage methodStorage = new MethodStorage(method);

            if (Modifier.isFinal(method.getModifiers()))
                badMethods.add(methodStorage);

            if (Modifier.isPrivate(method.getModifiers()))
                badMethods.add(methodStorage);

            if (Modifier.isStatic(method.getModifiers()))
                badMethods.add(methodStorage);

            if (!map.containsKey(methodStorage)) {
                map.put(methodStorage, method);
            } else {
                Method currentMethod = map.get(methodStorage);
                if (currentMethod.getReturnType().isAssignableFrom(method.getReturnType()))
                    map.put(methodStorage, method);
            }
        }

        Class<?>[] interfaces = token.getInterfaces();
        Class<?> parent = token.getSuperclass();
        dfsMethods(parent, map, badMethods);
        for (Class<?> current : interfaces)
            dfsMethods(current, map, badMethods);
    }

    /**
     * Returns {@link Set} of {@link Method}, that contains in token class or in it's parents.
     * For get methods in specified class, we call {@link #dfsMethods(Class, java.util.Map, java.util.Set)}, that
     * recursively get methods from token class and from parents.
     * @param token class, wherein we get all methods
     * @return Set of methods that contains in token class.
     * @see Set
     */
    private Set<Method> getAllMethods(Class<?> token) {
        Map<MethodStorage, Method> map = new HashMap<>();
        Set<MethodStorage> badMethods = new HashSet<>();
        dfsMethods(token, map, badMethods);
        for (MethodStorage methodStorage : badMethods) {
            map.remove(methodStorage);
        }

        Set<Method> result = new HashSet<Method>(map.values());
        return result;
    }

    /**
     * Return an implementaion of all methods, that contains in token class and in it's
     * parents, that could be interface or another class
     * @param token class that could me implement
     * @return methods implementaion of class
     */
    private String getMethods(Class<?> token) {
        StringBuilder builder = new StringBuilder();
        Set<Method> methods = getAllMethods(token);
        for (Method method : methods) {
            builder.append(getMethodImplement(method));
        }
        return builder.toString();

    }

    /**
     * Method that provides class implementaion from it's {@link Class} token.
     * Result stores in specified file.
     * Generated class name should be same name of the token with <tt>Impl</tt> suffix added.
     * @param token type token to create implementation for.
     * @param root root directory.
     * @throws ImplerException if it's impossible to implement class
     * @see Class
     */
    @Override
    public void implement(Class<?> token, File root) throws ImplerException {

        if ( Modifier.isFinal(token.getModifiers())) {
            throw new ImplerException("Can't implement this class, because the class is final") ;
        }
        StringBuilder builder = new StringBuilder();
        className = token.getSimpleName() + "Impl";
        builder.append("package " + token.getPackage().getName() + ";\n");
        builder.append("public class" + " " + className + " " + getInheritance(token) + " {\n");
        try {
            builder.append(getConstructors(token) + "\n");
        } catch (ImplerException e) {
            throw new ImplerException("Can't implement this class");
        }
        builder.append(getMethods(token) + "\n");

        builder.append("}");

        try {
            String packageName = token.getPackage().getName().replace(".", "/");
            String pathToFile = root.toString() + "/" + packageName;
            boolean success = (new File(pathToFile)).mkdirs();
            File fileName = new File(pathToFile, className + ".java");
            PrintWriter writer = new PrintWriter(fileName);
            writer.write(builder.toString());
            writer.close();
        } catch (FileNotFoundException e) {
            throw new ImplerException("Class not found");
        }
    }

    /**
     * Method that provides specified class implementaion and stored result in
     * specified <tt>.jar</tt> file.
     * <p>
     *     For that, method created temporal directory and
     *     call {@link #implement(Class, java.io.File)}, which put result into
     *     our temporal directory.
     *
     *     Generated class name should be same name of the token with <tt>Impl</tt> suffix added.
     * </p>
     * @param token type token to create implementation for.
     * @param jarFile target <tt>.jar</tt> file.
     * @throws ImplerException
     */
    @Override
    public void implementJar(Class<?> token, File jarFile) throws ImplerException {
        String className = token.getSimpleName();
        String temporalDirString = className + ".temporal";
        boolean success = new File(temporalDirString).mkdir();
        if (!success)
            new ImplerException("Can't create temporal directory to create jar file. Please, check permissions");
        File temporalDir = new File(className + ".temporal");
        implement(token, temporalDir);
        compile(temporalDirString, token);
        createJarFile(temporalDir, jarFile);
        deleteDirectory(temporalDir);
    }

    /**
     * The method is trying to delete directory. For that, it going throw all files recursively
     * and delete each of them
     * @param directory that could be deleted
     */
    private void deleteDirectory(File directory) {
        File [] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDirectory(file);
            } else
                file.delete();
        }
        directory.delete();
    }

    /**
     * This method is creating <tt>.jar</tt> archive with files, which stores in specified directory.
     * Resulting jar archive would be stored in specified file. The method created the
     * object of {@link JarOutputStream} class, that provides creating jar archive.
     * <p>
     *     It's need to create {@link Manifest}, that indicates meta-information about
     *     resulting archive.
     * </p>
     * @param directory the root directory, where we start
     * @param jarFile is file that stored jar file after function executing
     * @see JarOutputStream
     * @see Manifest
     */
    private void createJarFile(File directory, File jarFile) {
        Manifest manifest = new Manifest();
        manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
        try (JarOutputStream target = new JarOutputStream(new FileOutputStream(jarFile), manifest))
        {
            for (File innerFile : directory.listFiles())
                addFileToJar(directory, innerFile, target);
        } catch (IOException e) {
            new ImplerException("Some problems with jarFile");
        }
    }

    /**
     * Recursively add all files in specified directory to <tt>.jar</tt> archive. Every file or directory
     * would be add to {@link JarEntry}, that represent <tt>.jar</tt> file entry.
     * <p>
     *      When it detects directory, then it starts to process all of the files recursively.
     *      All files must be copy to <tt>.jar</tt> archive with {@link BufferedInputStream}
     * </p>
     * @param directory the root directory, where we start
     * @param source is current file that could be add into JAR file
     * @param target is instance of {@link JarOutputStream} where the JAR file would be stored
     * @throws IOException If an input or output exception occurred
     * @see BufferedInputStream
     * @see JarEntry
     */
    private void addFileToJar(File directory, File source, JarOutputStream target) throws IOException {
        BufferedInputStream in = null;
        try {
            if (source.isDirectory()) {

                String name = directory.toPath().relativize(source.toPath()).toString();
                if (!name.isEmpty()) {
                    JarEntry entry = new JarEntry(name);
                    entry.setTime(source.lastModified());
                    target.putNextEntry(entry);
                    target.closeEntry();
                }
                for (File innerFile : source.listFiles())
                    addFileToJar(directory, innerFile, target);
                return;
            }

            JarEntry entry = new JarEntry(directory.toPath().relativize(source.toPath()).toString());
            entry.setTime(source.lastModified());
            target.putNextEntry(entry);
            in = new BufferedInputStream(new FileInputStream(source));

            byte[] buffer = new byte[1024];
            while (true) {
                int count = in.read(buffer);
                if (count == -1)
                    break;
                target.write(buffer, 0, count);
            }
            target.closeEntry();
        } finally {
            if (in != null)
                in.close();
        }
    }

    /**
     * Function that prepares file compiling.
     * @param prefix path in which the class is stored
     * @param token class that could be compiled
     */
    private void compile(String prefix, Class<?> token) {
        String path = prefix + "/" + token.getCanonicalName().replace(".", "/") + "Impl.java";
        compileJavaFile(new File(path).getAbsoluteFile());
    }

    /**
     * Function that compile java file
     * @param source that could be compiled
     * @see JavaCompiler
     */
    private void compileJavaFile(File source) {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        List<String> args = new ArrayList<>();
        args.add(source.getPath());
        int exitCode = compiler.run(null, null, null, args.toArray(new String[args.size()]));
    }

    /**
     * Main function that call when the class start executing
     * @param args arguments that passed into program
     */
    public static void main(String[] args) {
        if (args.length != 2 && args.length != 3) {
            System.out.println("Usage : \n" +
                                "To generate class implementaion run : " +
                                    "java -jar Implementor.jar className resultDir\n" +
                                "To generate .jar file with class implementaion run : " +
                                    "java -jar Implementor.jar -jar className resultFile");
            return;
        }
        String className;
        File root;
        if (args.length == 2) {
            className = args[0];
            root = new File(args[1]);
        } else {
            className = args[1];
            root = new File(args[2] + ".jar");
        }
        Implementor instance = new Implementor();
        try {
            File file = new File(".");
            URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {file.toURI().toURL()});
            Class<?> clazz = classLoader.loadClass(className);
            if (args.length == 2)
                instance.implement(clazz, root);
            else
                instance.implementJar(clazz, root);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ImplerException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
