cd src
javac ru/ifmo/ctddev/dronov/task3/Implementor.java
cd ..

manifest=./Manifest.txt

echo "Main-Class: ru.ifmo.ctddev.dronov.task3.Implementor" > "$manifest"

jar cfm Implementor.jar "$manifest" -C src/ .

rm -Rf "$manifest"
