package ru.ifmo.ctddev.dronov.task7;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 10.04.14
 * Time: 2:20
 * To change this template use File | Settings | File Templates.
 */

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Main class that call {@link Producer} to execute tasks
 */
public class Main {

    public static final int PRODUCER_COUNT = 10;
    public static final int WORKER_COUNT = 10;
    public static final int PUBLISHER_COUNT = 10;

    public static void main(String[] args) {

        BlockingQueue<TaskItem> workerQueue = new ArrayBlockingQueue<TaskItem>(WORKER_COUNT);
        BlockingQueue<Long> publishQueue = new ArrayBlockingQueue<Long>(PUBLISHER_COUNT);
        for (int i = 0; i < PRODUCER_COUNT; i++) {
            executeProducer(i, workerQueue);
        }

        for (int i = 0; i < WORKER_COUNT; i++) {
            executeWorker(i, workerQueue, publishQueue);
        }

        for (int i = 0; i < PUBLISHER_COUNT; i++) {
            executePublisher(i, publishQueue);
        }
    }

    private static void executePublisher(int id, BlockingQueue<Long> publishQueue) {
        Publisher publisher = new Publisher("publisher" + id, publishQueue);
        Thread thread = new Thread(publisher);
        thread.start();
    }

    private static void executeWorker(int id, BlockingQueue<TaskItem> workerQueue, BlockingQueue<Long> publishQueue) {
        Worker worker = new Worker("worker" + id, workerQueue, publishQueue);
        Thread thread = new Thread(worker);
        thread.start();
    }

    private static void executeProducer(int id, BlockingQueue<TaskItem> workerQueue) {
        Producer producer = new Producer("producer" + id, workerQueue);
        Thread thread = new Thread(producer);
        thread.start();
    }

}
