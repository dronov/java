package ru.ifmo.ctddev.dronov.task7;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 10.04.14
 * Time: 2:40
 * To change this template use File | Settings | File Templates.
 */

import java.util.concurrent.BlockingQueue;

/**
 * Class that print result of executed task
 */
public class Publisher implements Runnable {

    /**
     * Publisher name
     */
    private String publisherName;

    /**
     * Queue for publish result of executed tasks
     */
    BlockingQueue<Long> publisherQueue;

    public Publisher(String publisherName, BlockingQueue publisherQueue) {
        this.publisherName = publisherName;
        this.publisherQueue = publisherQueue;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                Long result = publisherQueue.take();
                System.out.println("Publisher " + publisherName + " : " + result);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
