package ru.ifmo.ctddev.dronov.task7;

/**
 * Class provides item for {@link java.util.concurrent.BlockingQueue}
 */
public class TaskItem<X, Y> {
    /**
     * Task that could be execute
     */
    private Task<X, Y> task;
    /**
     * Input argument
     */
    private Y value;

    /**
     *
     */
    private X result;
    /**
     * Flag that show result of execute task
     */
    public boolean isFinished;

    /**
     * Class constructor
     * @param task that will be execute
     * @param value input value
     */
    public TaskItem(Task<X, Y> task, Y value) {
        this.task = task;
        this.value = value;
    }

    /**
     * Method that execute task
     */
    public void execute() {
        result = task.run(value);
        isFinished = true;
    }

    /**
     * Method that return result of executed task
     * @return result of executed task
     */
    public X getResult() {
        return result;
    }
}
