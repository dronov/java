package ru.ifmo.ctddev.dronov.task7; /**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 10.04.14
 * Time: 2:21
 * To change this template use File | Settings | File Templates.
 */

import java.util.Random;
import java.util.concurrent.BlockingQueue;

/**
 * Class that generate {@link ru.ifmo.ctddev.dronov.task7.Task}
 */
public class Producer implements Runnable {

    /**
     * Name of current {@link Producer}
     */
    String producerName;

    /**
     * Queue of generated tasks for {@link Worker}
     */
    BlockingQueue<TaskItem> workerQueue;

    public Producer(String producerName, BlockingQueue<TaskItem> workerQueue) {
        this.producerName = producerName;
        this.workerQueue = workerQueue;
    }

    @Override
    public void run() {
        Random random = new Random();
        while (!Thread.currentThread().isInterrupted()) {
            Task<Long, Integer> currentTask = generateTask();
            int value = random.nextInt(12);
            TaskItem<Long, Integer> taskItem = new TaskItem<Long, Integer>(currentTask, value);
            try {
                System.out.println("Producer : " + producerName + " generated task with value " + value);
                workerQueue.put(taskItem);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Method that generated tasks
     * @return generated task
     */
    private Task<Long, Integer> generateTask() {
        return new Task < Long, Integer >()  {
            /**
             * Calculate generated task
             * @param value input argument
             * @return factorial of value
             */
            @Override
            public Long run(Integer value) {
                Long result = (long)1;
                for (int i = 2; i <= value; i++) {
                    result *= i;
                }
                return result;
            }
        };
    }
}
