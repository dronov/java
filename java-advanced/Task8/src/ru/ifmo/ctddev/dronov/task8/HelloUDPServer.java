package ru.ifmo.ctddev.dronov.task8;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 24.04.14
 * Time: 1:59
 */
public class HelloUDPServer {

    private static final int THREADS = 10;
    private static final int BUFFER_SIZE = 512;
    private final int port;
    private Executor executor;

    public HelloUDPServer(int port) {
        this.port = port;
    }

    private class ExecutePacket implements Runnable {

        private DatagramPacket packet;
        public ExecutePacket(DatagramPacket packet) {
            this.packet = packet;
        }

        @Override
        public void run() {
            try (DatagramSocket socket = new DatagramSocket()) {
                ByteArrayInputStream inputStream = new ByteArrayInputStream(packet.getData());
                try (DataInputStream input = new DataInputStream(inputStream)) {
                    String request = input.readUTF();
                    System.out.println("Received " + request);
                    String answer = "Hello, " + request;

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(BUFFER_SIZE);
                    try (DataOutputStream output = new DataOutputStream(outputStream)) {
                        output.writeUTF(answer);
                        byte[] buffer = outputStream.toByteArray();
                        DatagramPacket answerPacket = new DatagramPacket(buffer, 0, buffer.length, packet.getSocketAddress());
                        socket.send(answerPacket);
                        System.out.println("Send socket with answer " + answer);

                    } catch (IOException e) {
                        System.err.println("Some problem with sending answer packet " + e.getMessage());
                    }

                } catch (IOException e) {
                    System.err.println("Some problem with received socket" + e.getMessage());
                }
            } catch (SocketException e) {
                System.err.print("Some problem with creating socket " + e.getMessage());
            }
        }
    }


    private void start() throws ServerException {
        executor = Executors.newFixedThreadPool(THREADS);
        try (DatagramSocket socket = new DatagramSocket(port)) {
            while (!Thread.interrupted()) {
                try {
                    byte [] buffer = new byte[BUFFER_SIZE];
                    DatagramPacket packet = new DatagramPacket(buffer, 0, BUFFER_SIZE);
                    socket.receive(packet);
                    executor.execute(new ExecutePacket(packet));
                } catch (IOException e) {
                    System.err.print("Some problem with received packet\n");
                }
            }
        } catch (SocketException e) {
            throw new ServerException("Some problem with creating socket " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage : you need pass 1 argument\n 1)Port number");
        } else {
            try {
                int port = Integer.valueOf(args[0]);
                HelloUDPServer server = new HelloUDPServer(port);
                server.start();
            } catch (NumberFormatException e) {
                System.err.print("Port should be an integer");
            } catch (ServerException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }
}
