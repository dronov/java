package ru.ifmo.ctddev.dronov.task8;

import java.io.*;
import java.net.*;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 24.04.14
 * Time: 1:11
 */

public class HelloUDPClient {
    public static final int THREADS = 10;

    private final Thread[] threads;
    private final String prefix;
    private SocketAddress address;

    public HelloUDPClient(String serverName, int port, String prefix) throws ClientException {
        this.prefix = prefix;
        this.threads = new Thread[THREADS];
        try {
            address = new InetSocketAddress(InetAddress.getByName(serverName), port);
        } catch (UnknownHostException e) {
            throw new ClientException("Unknown host " + serverName);
        }
    }


    private class Forwarding implements Runnable {
        private static final int BUFFER_SIZE = 512;
        private String threadId;

        public Forwarding(String threadId) {
            this.threadId = threadId;
        }

        @Override
        public void run() {
            int count = 0;
            while (!Thread.currentThread().isInterrupted()) {

                count++;
                try (DatagramSocket socket = new DatagramSocket()) {
                    String request = prefix + "_" + threadId + "_" + count;

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(BUFFER_SIZE);

                    try (DataOutputStream output = new DataOutputStream(outputStream)) {
                        output.writeUTF(request);
                        byte[] buffer = outputStream.toByteArray();
                        DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length, address);
                        socket.send(packet);

                        System.out.println("Send to server a request " + request);

                        byte[] incomingBuffer = new byte[BUFFER_SIZE];
                        ByteArrayInputStream inputStream = new ByteArrayInputStream(incomingBuffer, 0, BUFFER_SIZE);

                        try (DataInputStream input = new DataInputStream(inputStream)) {
                            DatagramPacket incomingPacket = new DatagramPacket(incomingBuffer, 0, BUFFER_SIZE);
                            socket.receive(incomingPacket);

                            String answer = input.readUTF();
                            System.out.println("Received answer " + answer);
                        } catch (Exception e) {
                            System.err.print("Some problem with received packets " + e.getMessage());
                        }
                    } catch (IOException e) {
                        System.err.print("Some problem with sending packets " + e.getMessage());
                    }
                } catch (SocketException e) {
                    System.err.print("Some problem with creating sockets " + e.getMessage());
                }
            }
        }
    }

    private void start() {
        for (int i = 0; i < THREADS; i++) {
            threads[i] = new Thread(new Forwarding(Integer.toString(i)));
        }
        for (int i = 0; i < THREADS; i++)
            threads[i].start();
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Usage : you need to pass 3 arguments\n 1)Server's name or IP adress\n" +
                    "2)Port number\n" +
                    "3)Queries");
        } else {
            try {
                String serverName = args[0];
                int port = Integer.valueOf(args[1]);
                String prefix = args[2];

                HelloUDPClient client = new HelloUDPClient(serverName, port, prefix);
                client.start();
            } catch (NumberFormatException e) {
                System.err.print("Port should be an integer");
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }

    }

}


