package ru.ifmo.ctddev.dronov.task8;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 24.04.14
 * Time: 2:02
 */
public class ServerException extends Exception {

    public ServerException(String message) {
        super(message);
    }
}
