package ru.ifmo.ctddev.dronov.task8;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 24.04.14
 * Time: 1:22
 */
public class ClientException extends Exception {

    public ClientException(String message) {
        super(message);
    }

}
