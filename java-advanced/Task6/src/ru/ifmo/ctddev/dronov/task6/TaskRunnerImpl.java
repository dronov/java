package ru.ifmo.ctddev.dronov.task6;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Class that run {@link Task}'s
 */
public class TaskRunnerImpl implements TaskRunner {
    /**
     * Array of threads
     */
    private Thread[] threads;
    /**
     * Task queue
     */
    private Queue<TaskItem> queue;


    /**
     * Task Runner constructor
     * @param count number of threads
     * @see Thread
     */
    public TaskRunnerImpl(int count) {
        threads = new Thread[count];
        queue = new ArrayDeque<TaskItem>(count);

        for (int i = 0; i < count; i++)
            threads[i] = new Thread(new Runner(), "TaskRunner " + i);
        for (int i = 0; i < count; i++) {
            threads[i].start();
        }
    }

    @Override
    public <X, Y> X run(Task<X, Y> task, Y value) {
        TaskItem<X, Y> item = new TaskItem<X, Y>(task, value);
        System.out.println(Thread.currentThread().getName() + " want to put task to queue ");

        synchronized (queue) {
            queue.add(item);
            queue.notify();
        }

        synchronized (item) {
            while (!item.isFinished) {
                try {
                    item.wait();
                } catch (InterruptedException e) {
                    System.out.println("Task was interrupted");
                    e.printStackTrace();
                }
            }
        }
        System.out.println(Thread.currentThread().getName() + " get the result ");
        return item.getResult();
    }

    private class Runner implements Runnable {

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    System.out.println(Thread.currentThread().getName() + " want to execute task ");
                    TaskItem headTask;
                    synchronized (queue) {
                        while (queue.isEmpty())
                            queue.wait();
                        headTask = queue.poll();
                    }
                    System.out.println(Thread.currentThread().getName() + " get the task ");
                    synchronized (headTask) {
                        headTask.execute();
                        headTask.notify();
                    }
                    System.out.println(Thread.currentThread().getName() + " executed ");
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

}
