package ru.ifmo.ctddev.dronov.task6;


import java.util.Random;

/**
 * This class generate {@link Task} and execute with {@link TaskRunner}
 */
public class Client {
    /**
     * Current client name for mapping into display
     */
    private String clientName;

    /**
     * Current {@link TaskRunner} that will be execute generating tasks
     */
    private TaskRunner taskRunner;

    /**
     * Client's class constructor
     * @param clientName client name
     * @param taskRunner Task Runner that could be execute tasks
     */
    public Client(String clientName, TaskRunner taskRunner) {
        this.clientName = clientName;
        this.taskRunner = taskRunner;
    }

    /**
     * Method that generates tasks in infinity loop. For generate task
     * method using {@link ru.ifmo.ctddev.dronov.task6.Client#generateTask()}
     */
    public void evaluate() {
        new Thread(clientName) {
            @Override
            public void run() {
                Random random = new Random();
                while (true) {
                    Task<Long, Integer> currentTask = generateTask();
                    try {
                        System.out.println(clientName + " " + taskRunner.run(currentTask, random.nextInt(12)));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    /**
     * Method that generated tasks for {@link ru.ifmo.ctddev.dronov.task6.Client#evaluate()}
     * @return generated task
     */
    private Task<Long, Integer> generateTask() {
        return new Task < Long, Integer >()  {
            /**
             * Calculate generated task
             * @param value input argument
             * @return factorial of value
             */
            @Override
            public Long run(Integer value) {
                Long result = (long)1;
                for (int i = 2; i <= value; i++) {
                    result *= i;
                }
                return result;
            }
        };
    }

}
