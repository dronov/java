package ru.ifmo.ctddev.dronov.task6;

/**
 * Created with IntelliJ IDEA.
 * User: dronov
 * Date: 10.04.14
 * Time: 1:49
 * To change this template use File | Settings | File Templates.
 */

import java.util.Random;

/**
 * Main class that run Client-TaskRunner
 */
public class Main {

    /**
     * Number of task runner
     */
    public static final int TASK_RUNNER_NUMBER = 5;

    /**
     * Number of threads in task runer
     */
    public static final int THREAD_NUMBER_IN_TASK_RUNNER = 5;

    /**
     * Number of clients
     */
    public static final int NUMBER_OF_CLIENTS = 5;

    public static void main(String[] args) {
        TaskRunner [] taskRunner = new TaskRunnerImpl[TASK_RUNNER_NUMBER];
        for (int i = 0; i < TASK_RUNNER_NUMBER; i++)
            taskRunner[i] = new TaskRunnerImpl(THREAD_NUMBER_IN_TASK_RUNNER);

        Client [] clients = new Client[NUMBER_OF_CLIENTS];
        Random random = new Random();
        for (int i = 0; i < NUMBER_OF_CLIENTS; i++) {
            clients[i] = new Client("Client # " + i, taskRunner[random.nextInt(TASK_RUNNER_NUMBER)]);
            clients[i].evaluate();
        }
    }
}
