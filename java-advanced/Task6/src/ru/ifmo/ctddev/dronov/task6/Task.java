package ru.ifmo.ctddev.dronov.task6;


/**
 * Interface that provides calculating the task and get result of evaluating
 * @param <X> result type
 * @param <Y> argument type
 */
public interface Task<X, Y> {
    /**
     * This method calculates the task
     * @param value input argument
     * @return evaluated value
     */
    X run(Y value);
}
