package ru.ifmo.ctddev.dronov.task6;

/**
 * Interface that run the task
 */
public interface TaskRunner {
    /**
     * Method that will be run on specified task and input value
     * @param task specified task that could be evaluate
     * @param value input value
     * @param <X> result type
     * @param <Y> input type
     * @return result of execute
     */
    <X, Y> X run(Task<X, Y> task, Y value) throws InterruptedException;
}
